package com.example.contactapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        val navigationView = findViewById<NavigationView>(R.id.navigation_view)

        navigationView.setNavigationItemSelectedListener { menuItem ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_CONTACTS
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.READ_CONTACTS),
                        PERMISSIONS_REQUEST_READ_CONTACTS
                    )
                } else {
                    when (menuItem.itemId) {
                        R.id.listView -> {
                            supportFragmentManager.beginTransaction()
                                .replace(R.id.fragment_container, ListFragment())
                                .commit()
                        }
                        R.id.recyclerView -> {
                            supportFragmentManager.beginTransaction()
                                .replace(R.id.fragment_container, RecyclerViewFragment())
                                .commit()
                        }
                    }
                    drawerLayout.closeDrawers()
                }
            } else {
                when (menuItem.itemId) {
                    R.id.listView -> {
                        supportFragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, ListFragment())
                            .commit()
                    }
                    R.id.recyclerView -> {
                        supportFragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, RecyclerViewFragment())
                            .commit()
                    }
                }
                drawerLayout.closeDrawers()
            }
            true
        }
    }

    companion object {
        private const val PERMISSIONS_REQUEST_READ_CONTACTS = 1
    }

//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        when (requestCode) {
//            PERMISSIONS_REQUEST_READ_CONTACTS -> {
//                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(this, "Get contacts ....", Toast.LENGTH_LONG).show()
//                } else {
//                    Toast.makeText(this, "You have disabled a contacts permission", Toast.LENGTH_LONG).show()
//                }
//                return
//            }
//        }
//    }
}