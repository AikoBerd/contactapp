package com.example.contactapp

data class Contact(
    val name: String,
    val phoneNumber: String,
    val image: String?
)
