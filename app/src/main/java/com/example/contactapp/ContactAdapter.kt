package com.example.contactapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class ContactAdapter(
    private val contacts: List<Contact>,
    private val onItemClickListener: ((String) -> Unit)? = null
) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item, parent, false)
        return ViewHolder(itemView, onItemClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(contacts[position])
    }

    override fun getItemCount() = contacts.size

}

class ViewHolder(itemView: View, private val onItemClickListener: ((String) -> Unit)? = null) : RecyclerView.ViewHolder(itemView) {
    private val name: TextView = itemView.findViewById(R.id.name)
    private val phone: TextView = itemView.findViewById(R.id.phone)
    private val imageView: ImageView = itemView.findViewById(R.id.image_view)

    fun onBind(contacts: Contact){
        name.text = contacts.name
        phone.text = contacts.phoneNumber

        Glide.with(itemView.context)
            .load(contacts.image)
            .placeholder(R.drawable.ic_baseline_person)
            .into(imageView)

        phone.setOnClickListener {
            onItemClickListener?.invoke(contacts.phoneNumber)
        }
    }
}