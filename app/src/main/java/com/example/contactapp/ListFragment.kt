package com.example.contactapp

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide

class ListFragment : Fragment() {

    private lateinit var listView: ListView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        listView = view.findViewById(R.id.contact_list_view)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val cursor = requireActivity().contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            null,
            null,
            null
        )

        val adapter = SimpleCursorAdapter(
            requireContext(),
            R.layout.item,
            cursor,
            arrayOf(
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.PHOTO_URI),
            intArrayOf(R.id.name, R.id.phone, R.id.image_view),
            0
        )

        adapter.setViewBinder { view, cursor, columnIndex ->
            if (view is ImageView && columnIndex == cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)) {
                val imageUri = cursor.getString(columnIndex)
                if (!cursor.isNull(columnIndex)) {
                    Glide.with(view.context)
                        .load(imageUri)
                        .placeholder(R.drawable.ic_baseline_person)
                        .into(view)
                }
                true
            } else { false }
        }

        listView.adapter = adapter

        listView.setOnItemClickListener { _, _, _, _ ->
            val phoneNumberColumnIndex = cursor?.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            val phoneNumber = phoneNumberColumnIndex?.let { cursor.getString(it) }
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNumber"))
            startActivity(intent)
        }
    }
}